$(document).ready(function () {
    InitLazyLoad();
    CalculateTopMenuShadow();
    InitSmoothScroll();
    InitMenuButton();
    InitFinalCountdown();
    InitCarousel();
    ValidateFormFields();
    $(document).foundation();
    FormMask();
    InitMapsAPI();
    InitJivositeWidget();
    InitYandexMetrics()
});

$(window).bind("load", function () {
    $("img.lazy").trigger("sporty")
});

$(window).scroll(function(){
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function(){
        CalculateTopMenuShadow()
    }, 100)
});

function CalculateTopMenuShadow() {
    if ($(window).scrollTop() == 0)
    {
        $("#topMenuContainer").removeClass('shadow');
    }
    else
    {
        $("#topMenuContainer").addClass('shadow');
    }
}

function InitLazyLoad(){
    $("img.lazy").lazyload({
        effect: "fadeIn",
        effectTime: 500,
        event: "sporty"
    })
}

function InitMapsAPI(){
    $(window).load(function(){
        var script = document.createElement('script');
        script.src = "http://api-maps.yandex.ru/2.0/?load=package.standard&lang=ru-RU";
        document.body.appendChild(script);

        script.onload = function() {
            InitMap();
        }
    })
}

function InitMap(){
    var myMap;
    ymaps.ready(init);
    function init () {
        myMap = new ymaps.Map('map', {
            center: [56.471576, 47.883555],
            zoom: 9
        });
        myMap.controls
            .add('zoomControl', { left: 5, top: 5 })
            .add('typeSelector')
            .add('mapTools', { left: 35, top: 5 });

        var myPlacemark = new ymaps.Placemark([56.460223, 47.877653], {
            balloonContentBody: "оз. Шап"
        });

        myMap.geoObjects.add(myPlacemark);
    }
}

function InitSmoothScroll(){
    $('.smooth').smoothScroll({offset: -100});
}

function InitMenuButton() {
    var menuButton = $('#topMenuButton');
    var menuToToggle = $('#topmenuToToggle');

    menuButton.click(function() {
        menuToToggle.slideToggle(300);
        /* Animation if not IE <=9 */
        if (!document.all && window.atob)
        {
            (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
        }
    });

    menuToToggle.find('a').click(function() {
        if (menuButton.css('display') == 'block')
        {
            menuButton.click();
        }
    })
}

function InitFinalCountdown(){
    var counterDay = $("#counterDay");
    var counterHour = $("#counterHour");
    var counterMinute = $("#counterMinute");

    $('#counterContainer').countdown('2016/07/15', function (event) {
        counterDay.html(event.strftime('%D'));
        counterHour.html(event.strftime('%H'));
        counterMinute.html(event.strftime('%M'))
    })
}

function FormMask() {
    $(function(){
        $("input[data-type=phone]").inputmask("+7 (999) 999-99-99", {
            "clearIncomplete": true
        });

        $("input[data-type=email]").inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-zА-Яа-яЁё!#$%&'*+/=?^_`{|}~\-]",
                    cardinality: 1,
                    casing: "lower"
                }
            }
        });

        $("input[data-type=name]").inputmask({
            mask: "*{1,50}",
            placeholder: "",
            greedy: false,
            definitions: {
                '*': {
                    validator: "[A-Za-zА-Яа-яЁё -]"
                }
            }
        })
    })
}

function InitCarousel(){
    var jcarousel = $('.jcarousel');

    jcarousel
        .on('jcarousel:reload jcarousel:create', function () {
            var carousel = $(this),
                width = carousel.innerWidth();

            if (width >= 700) {
                width = width / 3;
            } else if (width >= 350) {
                width = width / 2;
            }

            carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
        })
        .jcarousel({
            wrap: 'circular'
        });

    $('.jcarousel-control-prev')
        .jcarouselControl({
            target: '-=1'
        });

    $('.jcarousel-control-next')
        .jcarouselControl({
            target: '+=1'
        });

    $('.jcarousel-pagination')
        .on('jcarouselpagination:active', 'a', function () {
            $(this).addClass('active');
        })
        .on('jcarouselpagination:inactive', 'a', function () {
            $(this).removeClass('active');
        })
        .on('click', function (e) {
            e.preventDefault();
        })
        .jcarouselPagination({
            perPage: 1,
            item: function (page) {
                return '<a href="#' + page + '">' + page + '</a>';
            }
        })
}

function ValidateFormFields(){
    $("form").find("input").focus(function(){
        $(this).removeClass("error");
    });
    $("form").submit(CheckForEmptyFields);
}

function CheckForEmptyFields(event){
    event.preventDefault();
    var error = false;

    var form = $(this);
    form.find("input").each(function(){
        if (this.value == '')
        {
            $(this).addClass('error');
            error = true
        }
    });

    if (!error)
    {
        if (form.attr('name') == 'order_ticket_infoport')
        {
            yaCounter29693990.reachGoal('bronirovanie')
        }
        SendFormValues(form)
    }
}

function SendFormValues(form){
    var data = form.serializeArray();
    $.ajax({
            type: 'POST',
            url: '/bitrix/form_submit_ajax_handler.php',
            data: data,
            success: function(response) {
                if (response == '')
                {
                    if (form.attr('name') == 'order_call_infoport')
                    {
                        $("#requestCall").find('.close-button').click()
                    }
                    else
                    {
                        alert('Ваша заявка успешно отправлена');
                        form.find("input[type=text]").each(function(){
                            $(this).val('')
                        })
                    }
                }
                else if (response == 'Ошибка! Введен некорректный адрес email')
                {
                    $("input[data-type=email]").addClass("error")
                }
                else
                {
                    alert(response)
                }
            }
        }
    )
}

function InitJivositeWidget() {
    var widget_id = 'i4E9HD5wAI';
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = '//code.jivosite.com/script/widget/' + widget_id;
    var ss = document.getElementsByTagName('script')[0];
    ss.parentNode.insertBefore(s, ss)
}

function InitYandexMetrics(){
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter29693990 = new Ya.Metrika({
                    id:29693990,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks")
}